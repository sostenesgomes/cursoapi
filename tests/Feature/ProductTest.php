<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{

    public function testGetAllProducts()
    {
        $response = $this->get('/api/products');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'title',
                    'stock',
                    'status',
                    'status_title'
                ]
            ]
        ]);
    }

    public function testProductNotFound()
    {
        $response = $this->get('/api/products/9999');

        $response->assertStatus(404);
    }
}
