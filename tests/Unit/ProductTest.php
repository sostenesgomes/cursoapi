<?php

namespace Tests\Unit;

use App\Models\Catalog\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testIfValueIsTrue()
    {
        $product = new Product();

        $this->assertEquals(true, $product->testTrue());

    }
}
