<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Resources\Catalog\CategoryResource;
use App\Models\Catalog\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CategoryResource::collection(Category::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $validator = $this->validator($data);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            return response()->json(Category::create($data));

        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $category = Category::find($id);

            if (! $category) {
                return response()->json(['errors' => ['category' => 'Category Not Found']], 404);
            }

            return new CategoryResource($category);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $category = Category::find($id);

            if (! $category) {
                return response()->json(['errors' => ['category' => 'Category Not Found']], 404);
            }

            $data = $request->all();
            $validator = $this->validator($data);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            $category->fill($data)->save();

            return response()->json($category);

        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $category = Category::find($id);

            if (! $category) {
                return response()->json(['errors' => ['category' => 'Category Not Found']], 404);
            }

            $category->delete();

            return response('', 204);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'        => 'required|string',
            'status'      => 'required|integer|in:1,2',
        ]);
    }
}
