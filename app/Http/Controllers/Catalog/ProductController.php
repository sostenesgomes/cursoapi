<?php

namespace App\Http\Controllers\Catalog;

use App\Http\Resources\Catalog\ProductResource;
use App\Models\Catalog\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProductResource::collection(Product::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $data = $request->all();
            $validator = $this->validator($data);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            return response()->json(Product::create($data));

        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $product = Product::find($id);

            if (! $product) {
                return response()->json(['errors' => ['product' => 'Product Not Found']], 404);
            }

            return new ProductResource($product);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $product = Product::find($id);

            if (! $product) {
                return response()->json(['errors' => ['product' => 'Product Not Found']], 404);
            }

            $data = $request->all();
            $validator = $this->validator($data);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            $product->fill($data)->save();

            return response()->json($product);

        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $product = Product::find($id);

            if (! $product) {
                return response()->json(['errors' => ['product' => 'Product Not Found']], 404);
            }

            $product->delete();

            return response('', 204);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * @param array $data
     * @return mixed
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'title'  => 'required|string',
            'stock'  => 'required|integer',
            'status' => 'required|integer|in:1,2',
            'category_id' => 'required|integer|exists:categories,id'
        ]);
    }
}
