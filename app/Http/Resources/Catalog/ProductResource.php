<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\Resource;

class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'title'        => $this->title,
            'stock'        => $this->stock,
            'status'       => $this->status,
            'status_title' => $this->status_title,
            'category'     => $this->when($request->has('category'), function() {
                return $this->category;
            })
        ];
    }
}
