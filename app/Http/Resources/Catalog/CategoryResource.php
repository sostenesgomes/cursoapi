<?php

namespace App\Http\Resources\Catalog;

use Illuminate\Http\Resources\Json\Resource;

class CategoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'name'         => $this->name,
            'status'       => $this->status,
            'status_title' => $this->status_title,
            'products'     => $this->when($request->has('products'), function (){
                return $this->products;
            })
        ];
    }
}
