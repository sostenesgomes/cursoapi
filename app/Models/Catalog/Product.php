<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $statuses = [
        1 => 'Ativo',
        2 => 'Inativo'
    ];

    /**
     * @var array
     */
    protected $fillable = ['id', 'title', 'category_id', 'stock', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return mixed
     */
    public function getStatusTitleAttribute()
    {
        return $this->statuses[$this->status];
    }

    public function testTrue()
    {
        return true;
    }

}
