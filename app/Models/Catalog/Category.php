<?php

namespace App\Models\Catalog;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * @var array
     */
    protected $statuses = [
        1 => 'Ativo',
        2 => 'Inativo'
    ];

    /**
     * @var array
     */
    protected $fillable = ['name', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function getStatusTitleAttribute()
    {
        return $this->statuses[$this->status];
    }
}
