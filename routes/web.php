<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('consumeapi', function (){
    $uri = 'https://jsonplaceholder.typicode.com/posts/1';

    $client = new \GuzzleHttp\Client();

    $response = $client->request('GET', $uri);

    $json = $response->getBody()->getContents();

    print $json;
});