<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Catalog\Product::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'stock' => rand(10, 1000),
        'category_id' => \App\Models\Catalog\Category::inRandomOrder()->first()->id,
        'status' => 1
    ];
});
