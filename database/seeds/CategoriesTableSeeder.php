<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            ['name' => 'Eletrônicos', 'status' => 1],
            ['name' => 'Informática', 'status' => 1]
        ];

        foreach ($categories as $category) {
            $findCategory = \App\Models\Catalog\Category::where('name', $category['name'])->first();

            if (! $findCategory) {
                \App\Models\Catalog\Category::create($category);
            }
        }
    }
}
